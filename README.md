## Product Management

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Installation and Setup Instructions

### Dependencies:

Product Management service

## Available Scripts

In the project root directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3005](http://localhost:3005) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


