
import React, {useState, useEffect, createContext} from 'react';
import ProductApi from '../api/ProductApi';

export const ProductContext = createContext();

export const ProductProvider = props => {

    const [products, setProducts] = useState([]);

    const fetchProducts = async () => {
        const response = await ProductApi.get("/products");
        setProducts(response.data.content)
    }
    
    useEffect(() => {
        fetchProducts();
    },[])

    return(
        <ProductContext.Provider value={[products,setProducts]}>
            {props.children}
        </ProductContext.Provider>
    );
    
}