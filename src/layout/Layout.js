import { Menu, Breadcrumb, Select} from 'antd';

import SiderBar from '../sider/SideBar';

import { TitleContainer, ProductTitle } from './style';
import '../global/index.css'
import 'antd/dist/antd.css';
import logo from '../../src/components/img/logo.png';

import {Content, Container, Banner, LogoImage} from './style'

const { SubMenu } = Menu;
const Layout = ({children}) =>{
  return(
    <Container>
      <SiderBar />
      <Content style={{ padding: '0 24px', minHeight: 280 }}>
        <Breadcrumb style={{ margin: '16px 0' }}> 
        </Breadcrumb>
        <Banner className="header">
          <LogoImage src={logo} alt="Leisure Pass Group Logo" />
        </Banner>
        <TitleContainer>
          <ProductTitle>Product Management</ProductTitle>
        </TitleContainer>
         {children}
        </Content>
    </Container>
    )
}

export default Layout;