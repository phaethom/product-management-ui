import ProductApi from "../api/ProductApi";

export const fetchDynamic = async (pageNr, size, sortBy, order) => {  
   const response= await ProductApi.get("products?page="+pageNr+"&size="+size+"&sort="+sortBy+   ","+order);
   return response;
}

export const deleteProduct =  async (producutId) => {
    const response = await ProductApi.delete("/products/" + producutId)
    console.log(response);
}

export const saveProduct = async (product) => {
    const response = await ProductApi.post("/products", product);
    console.log(response);
}