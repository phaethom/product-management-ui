
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import Home from './components/home/Home';
import { ProductProvider } from './provider/ProductContext';
import ProductList from './components/lists/ProductList';
import AddProduct from './components/addProduct/AddProduct';
import Layout from './layout/Layout';

function App() {
  return (
    <ProductProvider>
     <Router>
       <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route  path="/products" component={ProductList} />
          <Route path="/add" component={AddProduct} />
                  {/* <Route path="/edit/:id" component={AddProduct} /> */}
        </Switch>
      </Layout>
    </Router>
  </ProductProvider> 
  );
}

export default App;
