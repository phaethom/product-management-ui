import React, {useState} from 'react';
import 'antd/dist/antd.css';
import {Form, Input, DatePicker, Select, Button, Space} from 'antd';
import {Container} from './style'
import { saveProduct } from '../../service/ProductService';


const { Option } = Select;

const AddProduct = () => {

  const [purchasedDate, setPurchasedDate] = useState("");
  const [componentSize, setComponentSize] = useState('default');
  const [product, setProduct]= useState({
    name: "",
    description: "",
    categoryId: "",
    lastPurchasedDate: "",
    creationDate: ""
  });
    
  const handleCategory = (value) =>{
    product.categoryId = value;
  }

  const addProduct = () =>{
    saveProduct(product);
  }

  const handleFormData = (e) =>{
    e.preventDefault();
    const myCurrentDate = new Date();
    const newDate = myCurrentDate.getDate() + '/' + (myCurrentDate.getMonth()+1) + '/' + myCurrentDate.getFullYear() + ' '+ myCurrentDate.getHours()+':'+ myCurrentDate.getMinutes()+':'+ myCurrentDate.getSeconds();
    const newProduct={...product}
    newProduct[e.target.id] = e.target.value;
    newProduct.creationDate = newDate;
    newProduct.lastPurchasedDate = purchasedDate;
    setProduct(newProduct)
  }

  const handleLastPuchasedDate = (value, dateString) => {
    setPurchasedDate(dateString)
  }

  const onChangeLastPurch = (value, dateString) => {
    product.lastPurchasedDate = dateString;
  }
    
  const onOk=(value)=> {
    console.log('onOk: ', value);
  }

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  }

    return(
       <Container>
         <h2>Add Product</h2>
       <Form
       labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
       >
          <Form.Item label="name">
            <Input onChange={(e)=>handleFormData(e)} id="name" />
          </Form.Item>
          <Form.Item label="description">
            <Input onChange={(e)=>handleFormData(e)} id="description" />
          </Form.Item>
          <Form.Item label="Category">
            <Select onChange={(value)=>handleCategory(value) } >
              <Option value="1">Kitchen</Option>
              <Option value="2">Power Tools</Option>
              <Option value="3">Furniture</Option>
              <Option value="4">Electric</Option>
              <Option value="5">Washroom</Option>
              <Option value="6">Textiles</Option>
              <Option value="7">Misc.</Option>
            </Select>
          <Space direction="vertical"></Space>
          <Form.Item>
            <DatePicker showTime onChange={onChangeLastPurch} onOk={onOk} format="DD/MM/YYYY HH:mm:ss"/>
          </Form.Item>
          </Form.Item>
          <Form.Item>
          </Form.Item>
        <Form.Item >
         <Button type="primary" onClick={addProduct}>
             Save
         </Button>
          </Form.Item>
       </Form>
      </Container>
    )

}

export default AddProduct;