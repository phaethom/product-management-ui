import React, {useState, useContext, useEffect} from 'react';
import { SearchOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { List, Button, Popover, Popconfirm, Pagination, Select, Divider, Modal, Form, Input} from 'antd';
import { Container } from './style';
import { ProductContext } from '../../provider/ProductContext';
import 'antd/dist/antd.css';
import { deleteProduct, saveProduct } from '../../service/ProductService';
import ProductApi from '../../api/ProductApi';
import '../../global/index.css'

const ProductList = () => {

const { Option } = Select;       
  // const [products, setProducts] = useContext(ProductContext);
const [products, setProducts] = useState([]);
const [productId, setProductId]= useState("");
const [visible, setVisible] = useState(false);
const [confirmLoading, setConfirmLoading] = useState(false);
const [totalPages, setTotalPages]=useState(0)
const [elementsPerPage, setElementsPerPage]=useState(0)
const [pageNumber, setPageNumber] = useState(0)
const [sortBy, setSortBy]=useState("id")
const [orderBy, setOrderBy]=useState("desc")
const [deletedId, setDeletedId]= useState(0)
const [isModalVisible, setIsModalVisible] = useState(false);
const [productToEdit, setProductToEdit] = useState({});
const [toUpdateProduct, setToUpdateProduct]= useState({
  id: "",
  name: "",
  description: "",
  categoryId: "",
  lastPurchasedDate: "",
  creationDate: "",
  updatedDate: ""
});

const showModal = (item) => {
  //console.log(item)
  setIsModalVisible(true);
};

const handleOk2 = () => {
  console.log(toUpdateProduct)
saveProduct(toUpdateProduct);
setIsModalVisible(false);
};

const handleCancel2 = () => {
    setIsModalVisible(false);
};
  
useEffect(() => {
    
  fetchDynamic()
},[elementsPerPage, orderBy, sortBy, pageNumber,deletedId, productToEdit])


const handleFormData = (e) =>{
  e.preventDefault();
  const newProduct={...toUpdateProduct}
  newProduct.id = productToEdit.id;
  newProduct[e.target.id] = e.target.value;
  setToUpdateProduct(newProduct)
  console.log(newProduct)
  
}


const handleChooseEditItem= (item) => {
  setProductToEdit(item);
  if(productToEdit){
    setIsModalVisible(true);
  }
  console.log(item);

}

const fetchDynamic = async () => {
  const response= await ProductApi.get("products?page="+pageNumber+"&size="+elementsPerPage+"&sort="+sortBy+   ","+orderBy);
  setProducts(response.data.content)
  setTotalPages(response.data.totalPages)
}

  const showPopconfirm = () => {
    setVisible(true);
  };

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

 const handleDeleteProduct = (prodId) => {
  setDeletedId(prodId)
  deleteProduct(prodId);
  
  console.log(prodId)
 }

function handleItemsPerPage(value) {
  setElementsPerPage(value)
 }

const handleSortBy = (value) => {
  setSortBy(value)
}

const handleOrderBy = (value) => {
  setOrderBy(value)
}

const handleCategory = (value) =>{
  toUpdateProduct.categoryId = value;
}

const handlePagination = (page, pageSize) =>{
  setElementsPerPage(pageSize)
  setPageNumber(pageSize)
  console.log(page)
  console.log(pageSize)
}

 return(
  
<Container> 
  <h2>Products</h2>

  <Select defaultValue="5" style={{width: 120}} onChange={handleItemsPerPage}>
    <Option value="5">5/page</Option>
    <Option value="10">10/page </Option>
    <Option value="20">20/page </Option>
    
  </Select>
  <Select defaultValue="id" style={{ width: 120 }} onChange={handleSortBy}>
      <Option value="name">sort/id</Option>
      <Option value="name">sort/name</Option>
      <Option value="description">sort/description</Option>
      <Option value="categoryId">sort/category</Option>
    </Select>

    <Select defaultValue="desc" style={{ width: 120 }} onChange={handleOrderBy}>
      <Option value="desc">order/desc</Option>
      <Option value="asc">order/asc</Option>
    </Select>
  <List 
  bordered= {true}
  itemLayout= "horizontal"
  dataSource = {products}
  renderItem = {item => (
    <Popover content={<div>
      <p>{item.creationDate}</p>
      <p>{item.updatedDate}</p>
      <p>{item.lastPurchasedDate}</p>
    </div>} title="Dates">
    
   
    <List.Item  actions={[<Button onClick={() =>handleChooseEditItem(item)}    type="primary"  icon={<EditOutlined /> }>update</Button>, <Button onClick={() => handleDeleteProduct(item.id)} type="primary" danger icon={<DeleteOutlined />}>delete</Button>]}> 
   
      <List.Item.Meta 
      title = {item.name}
      description = {item.description}
      />
      
    </List.Item>
    </Popover>
  )}
  
  />
  <Pagination defaultCurrent={1} total={totalPages} pageSize={elementsPerPage} onChange={handlePagination}></Pagination>
  <Modal title="Update Product" visible={isModalVisible} onOk={handleOk2} onCancel={handleCancel2}>
    <Form size="middle" layout="horizontal" labelCol={{
          span: 4,
        }}>
    <Form.Item label="Id">
          <Input placeholder={productToEdit.id} onChange={(e)=>handleFormData(e)} id="id" />
        </Form.Item>
    <Form.Item label="Name">
          <Input placeholder={productToEdit.name} onChange={(e)=>handleFormData(e)} id="name" />
        </Form.Item>
        <Form.Item label="Description">
          <Input placeholder={productToEdit.description} onChange={(e)=>handleFormData(e)} id="description"/>
        </Form.Item>
        <Form.Item label="category Id">
        <Select onChange={(value)=>handleCategory(value) } >
              <Option value="1">Kitchen</Option>
              <Option value="2">Power Tools</Option>
              <Option value="3">Furniture</Option>
              <Option value="4">Electric</Option>
              <Option value="5">Washroom</Option>
              <Option value="6">Textiles</Option>
              <Option value="7">Misc.</Option>
          </Select>
         
        </Form.Item>
        <Form.Item label="Created">
          <Input placeholder={productToEdit.creationDate} onChange={(e)=>handleFormData(e)} id="creationDate"  />
        </Form.Item>
        <Form.Item label="Updated">
          <Input placeholder={productToEdit.updatedDate} onChange={(e)=>handleFormData(e)} id="updatedDate" />
        </Form.Item>
        <Form.Item label="Purchased">
          <Input placeholder={productToEdit.lastPurchasedDate} onChange={(e)=>handleFormData(e)} id="lastPurchasedDate" />
        </Form.Item>

    </Form>
        
      </Modal>

</Container>
    
 )
}
export default ProductList;