import React from 'react';
import {Menu} from 'antd';
import {Link} from 'react-router-dom'
import {  UserOutlined, LaptopOutlined, NotificationOutlined  } from '@ant-design/icons'
import {Container} from './style';

const {SubMenu} = Menu;
const SiderBar = () => {
    return(
        <Container  width={200}>
        <Menu
          mode="inline"
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          style={{ height: '100%' }}
        >
          <SubMenu key="sub1" icon={<UserOutlined />} title="John">
          <Menu.Item key="1" >Home <Link to="/"></Link> </Menu.Item>
            <Menu.Item key="2" >Product List <Link to="/products"></Link> </Menu.Item>
            <Menu.Item key="3">Add Product <Link to="/add"></Link> </Menu.Item>
            <Menu.Item key="4">Logout</Menu.Item>
          </SubMenu>

        </Menu>
      </Container>
    )
}

export default SiderBar;